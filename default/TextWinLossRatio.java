import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TextWinLossRatio {
	
	Game game;
	Player player;
	int balance;
	int bet;
	int limit;
	
	Dice die1;
	Dice die2;
	Dice die3;
	

	@Before
	public void setUp() throws Exception {
		balance = 100;
		bet = 5;
		limit = 0;
		die1 = new Dice();
		die2 = new Dice();
		die3 = new Dice();
		game = new Game(die1, die2, die3);
		player = new Player("Fred", balance);
		player.setLimit(limit);
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		//Arrange
		double ratio = 0.42;
		double MinRatio = ratio - (0.1 * ratio);
		double MaxRatio = ratio + (0.1 * ratio);
		double wins = 0;
		double numGames = 0;
		
		//Execute
		while (player.balanceExceedsLimitBy(bet) && player.getBalance() < 2 * balance){
			DiceValue pick = DiceValue.getRandom();
			int winnings = game.playRound(player, pick, bet);
			wins += (winnings > 0) ? 1 : 0;
			numGames++;
		}
		double TrueRatio = wins / numGames;
		double ratioAsDec = (double) Math.round(TrueRatio * 100) / 100;
		
		//Assert
		assertTrue(ratioAsDec >= MinRatio);
		assertTrue(ratioAsDec <= MaxRatio);
		
	}

}
