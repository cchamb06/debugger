import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestBetLimit {
	
	Game game;
	Player player;
	int bet;
	int balance;
	int limit;
	
	@Mock
	Dice die1;
	
	@Mock
	Dice die2;
	
	@Mock
	Dice die3;

	@Before
	public void setUp() throws Exception {
	balance = 5;
	bet = 5;
	limit = 0;
	game = new Game(die1, die2, die3);
	player = new Player("Fred", balance);
	player.setLimit(limit);
	}
	
	@Test
	public void test() {
		//Arrange
		DiceValue playerPick = DiceValue.CLUB;
		when(die1.getValue()).thenReturn(DiceValue.HEART);
		when(die2.getValue()).thenReturn(DiceValue.DIAMOND);
		when(die3.getValue()).thenReturn(DiceValue.CROWN);
		
		int expectedBalance = 0;
		
		while (player.balanceExceedsLimitBy(bet)){
			game.playRound(player, playerPick, bet);
			
		}
		
		assertTrue(player.getBalance() == expectedBalance);
		
		
	}

}

