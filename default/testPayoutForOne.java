import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class testPayoutForOne {
	
	Game game;
	Player player;
	int bet;
	int balance;
	int limit;
	int winnings;
	int winBalance;
	
	@Mock
	Dice die1;
	
	@Mock
	Dice die2;
	
	@Mock
	Dice die3;

	@Before
	public void setUp() throws Exception {
	balance = 100;
	bet = 5;
	limit = 100;
	int winnings = 5;
	winBalance = 105;
	game = new Game(die1, die2, die3);
	player = new Player("Fred", balance);
	player.setLimit(limit);
	}
	
	@Test
	public void test() {
		//Arrange
		DiceValue pick = DiceValue.HEART;
		when(die1.getValue()).thenReturn(DiceValue.HEART);
		when(die2.getValue()).thenReturn(DiceValue.DIAMOND);
		when(die3.getValue()).thenReturn(DiceValue.CROWN);
		
		
		
		//game.playRound(player, pick, bet);
		
		assertTrue(player.getBalance() == winBalance);
		
		
	}

}

